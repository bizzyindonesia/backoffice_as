package logistics.TestCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;		
//import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.net.URL;
//import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import logistics.library.Utility;

import logistics.Pages.FullLoginPage;
//import logistics.Pages.config;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author Bizzy
 */
public class OpenLogisticsApp  {		
	    private WebDriver driver;		
            	@Test				
		public void validLogin() throws Exception  {
	         WebDriver driver = new RemoteWebDriver(
	                   new URL("http://localhost:4444/wd/hub"), 
	                   DesiredCapabilities.firefox());
                 driver.get("http://staging-backoffice.bizzy.co.id");
                 
                FullLoginPage login= new FullLoginPage(driver);	
		login.verifyLogin("netsuite-dev@bizzy.co.id","bizzy2016!");
		login.validLoginAssert();
                System.out.println("Login verified 1!");
                
 
	        /*
                WebDriver augmentedDriver = new Augmenter().augment(driver);
	        File screenshot = ((TakesScreenshot)augmentedDriver).
	                            getScreenshotAs(OutputType.FILE);
	        */
                driver.quit();
	    }
}	
