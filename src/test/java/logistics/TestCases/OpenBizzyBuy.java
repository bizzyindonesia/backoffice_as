package logistics.TestCases;

import org.testng.annotations.Test;

import logistics.Pages.FullLoginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;	
import org.testng.annotations.AfterTest;		
public class OpenBizzyBuy {		
	    private WebDriver driver;
	    
	   
	     
	    
		@Test				
		public void myTest() throws Exception {
			//C:\Users\fadly\git\browserDriver
			System.setProperty("webdriver.gecko.driver","C:\\Users\\fadly\\git\\browserDriver\\geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver","D:\\TESTING\\eclipse\\browserDriver\\geckodriver.exe");
			//System.setProperty("webdriver.gecko.driver", "/home/bizzy/auto/backoffice_as/geckodriver");
			//System.setProperty("webdriver.gecko.driver", "/usr/local/share/chromedriver");
			
			
			/*
	        WebDriver driver = new RemoteWebDriver(
	                                new URL("http://103.78.25.220:4444/wd/hub"), 
	                                DesiredCapabilities.firefox());
			*/
			
			WebDriver driver= new FirefoxDriver();
			driver.manage().window().maximize();
			
	        driver.get("https://www.bizzy.co.id");
	        //FullLoginPage login= new FullLoginPage(driver);	
			//login.verifyLogin("netsuite-dev@bizzy.co.id","bizzy2016!");
	        
	        
	        // RemoteWebDriver does not implement the TakesScreenshot class
	        // if the driver does have the Capabilities to take a screenshot
	        // then Augmenter will add the TakesScreenshot methods to the instance
	        WebDriver augmentedDriver = new Augmenter().augment(driver);
	        File screenshot = ((TakesScreenshot)augmentedDriver).
	                            getScreenshotAs(OutputType.FILE);
	        driver.quit();
	    }
}	
