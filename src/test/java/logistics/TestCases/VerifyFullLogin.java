package logistics.TestCases;

import logistics.Pages.FullLoginPage;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
//import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

//@Listeners(ListenersDemo.TestNGListeners.class)
public class VerifyFullLogin 
{

	//WebDriver driver= new FirefoxDriver();
	
	
	@Test
	public void  validLogin()
	{
		WebDriver driver= new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://staging-logistics.bizzy.co.id/");
		
		FullLoginPage login= new FullLoginPage(driver);	
		login.verifyLogin("netsuite-dev@bizzy.co.id","bizzy2016!");
		login.validLoginAssert();	
	
	}
	
	@Test
	public void  invalidLogin()
	{
		WebDriver driver= new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://staging-logistics.bizzy.co.id/");
		
		FullLoginPage login= new FullLoginPage(driver);
		
		login.verifyLogin("fadlymahendra@gmail.com","v_way@88");
		login.invalidLoginAssert();
	}
	
}
