package logistics.TestCases;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;		
//import org.openqa.selenium.remote.RemoteWebDriver;
import java.io.File;
import java.net.URL;
import logistics.Pages.BuyerPage;
import logistics.Pages.CreateSOPage;
import logistics.Pages.CustomSOPage;
//import java.net.URL;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.remote.Augmenter;
import logistics.library.Utility;

import logistics.Pages.FullLoginPage;
import logistics.Pages.HomePage;
import logistics.Pages.SideBar;
//import logistics.Pages.config;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

/**
 *
 * @author Bizzy
 */
public class verifyUI_case  {		
	    private WebDriver driver;		
            	@Test				
		public void verifyUI() throws Exception  {
	         WebDriver driver = new RemoteWebDriver(
	                   new URL("http://localhost:4444/wd/hub"), 
	                   DesiredCapabilities.firefox());
                 driver.get("http://backoffice.bizzy.co.id");
                 
                FullLoginPage login= new FullLoginPage(driver);	
		login.verifyLogin("netsuite-dev@bizzy.co.id","bizzy2016!");
		login.validLoginAssert();
		
		HomePage home= new HomePage(driver);
		SideBar side= new SideBar(driver);
		
		home.clickMenuTriger();
		side.clickCreateSO();
		CreateSOPage create= new CreateSOPage(driver);
			create.uiCreateSO();
			System.out.println("uiCreateSO verified");
		
		home.clickMenuTriger();
		side.clickBuyer();
		BuyerPage buyer= new BuyerPage(driver);
		buyer.clickBuyingList();
			buyer.uiBuyingList();
			System.out.println("uiBuyingList verified");
			
		home.clickMenuTriger();
		side.clickCustomSO();
		CustomSOPage custom= new CustomSOPage(driver);
			custom.uiCustomSO();
			System.out.println("uiCustomSO verified");
			
		
		
		
		driver.quit();
		
	        /*
                WebDriver augmentedDriver = new Augmenter().augment(driver);
	        File screenshot = ((TakesScreenshot)augmentedDriver).
	                            getScreenshotAs(OutputType.FILE);
	        */
                driver.quit();
	    }
}	
