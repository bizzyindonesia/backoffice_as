package logistics.TestCases;

import logistics.Pages.BuyerPage;
import logistics.Pages.CreateSOPage;
import logistics.Pages.CustomSOPage;
import logistics.Pages.FullLoginPage;
import logistics.Pages.HomePage;
import logistics.Pages.SideBar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

public class verifyUI 
{
	@Test
	public void  verifyElement()
	{
		WebDriver driver= new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("http://backoffice.bizzy.co.id/");
		
		FullLoginPage login= new FullLoginPage(driver);	
		login.verifyLogin("netsuite-dev@bizzy.co.id","bizzy2016!");
		login.validLoginAssert();
		
		HomePage home= new HomePage(driver);
		SideBar side= new SideBar(driver);
		
		home.clickMenuTriger();
		side.clickCreateSO();
		CreateSOPage create= new CreateSOPage(driver);
			create.uiCreateSO();
			System.out.println("uiCreateSO verified");
		
		home.clickMenuTriger();
		side.clickBuyer();
		BuyerPage buyer= new BuyerPage(driver);
		buyer.clickBuyingList();
			buyer.uiBuyingList();
			System.out.println("uiBuyingList verified");
			
		home.clickMenuTriger();
		side.clickCustomSO();
		CustomSOPage custom= new CustomSOPage(driver);
			custom.uiCustomSO();
			System.out.println("uiCustomSO verified");
			
		
		
		
		driver.quit();
		
	}
	
	
}
