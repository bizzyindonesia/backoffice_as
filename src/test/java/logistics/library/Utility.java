package logistics.library;

import java.io.File;
//import java.io.IOException;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriverException;

public class Utility 
{

	
	public static void caputreScrenshot(WebDriver driver, String screenshotName)
	{
		try 
		{
			TakesScreenshot ts= (TakesScreenshot)driver;
			File source= ts.getScreenshotAs(OutputType.FILE);
			
			Calendar cal = Calendar.getInstance();
	        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
	        System.out.println( sdf.format(cal.getTime()) );
	        
	        
			FileUtils.copyFile(source, new File("./screenshots/"+screenshotName +".png"));
			
			System.out.println("Screenshots taken");
		} 
		catch (Exception e) 
		{
			System.out.println("Exception while taking screenshot" + e.getMessage());
		} 
		 	
	}
	
	
}
