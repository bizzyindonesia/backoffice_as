package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class CustomSOPage 
{

	WebDriver driver;
	
	
	public CustomSOPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
		
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,30)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	public void ExplicitWaitText(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.linkText(text)));
	}
		
	By SO_ID= By.linkText("3962");
	By newestSO= By.xpath(".//*[@id='salesorder-table-body']/tbody/tr[1]/td[1]/a");
	By qtySO= By.xpath(".//*[@id='table-so-scroll']/table/tbody/tr/td[7]");
	By split= By.linkText("Split");
	
	By qty= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[2]/td[2]/input");
	By qty2= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[3]/td[2]/input");
	By addNew= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[2]/td[4]/a/i");
	By remaining= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[4]/td[2]/input");
	By saveButton= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[4]/td[3]/a[1]");
	By resetButton= By.xpath(".//*[@id='sidepage']/div/div[1]/div[1]/a[2]");
	
	By customInfo= By.xpath(".//*[@id='table-so-scroll']/table/tbody/tr[1]/td[9]/span");
							 
	public void viewSO()
	{
		ExplicitWaitText(driver, "3962");
		driver.findElement(SO_ID).click();
		
	}
	 
	public void viewNewestSO()
	{
		ExplicitWait(driver, ".//*[@id='salesorder-table-body']/tbody/tr[1]/td[1]/a");
		driver.findElement(newestSO).click();
	}
	
	public void splitSO()
	{	
		driver.findElement(split).click();
		driver.findElement(qty).sendKeys("1");
		driver.findElement(addNew).click();
		
		String rem= driver.findElement(remaining).getAttribute("value");
		//String remRight= rem.substring(2,4);
		String remRight= rem.substring(rem.lastIndexOf("/") + 1);
		
		String remLeft= rem.substring(0,1);
		int rRight = Integer.parseInt(remRight);
		int rLeft = Integer.parseInt(remLeft);
		int dif= rRight - rLeft;
		String diff= Integer.toString(dif);	

		driver.findElement(qty2).sendKeys(diff);
		ExplicitWait(driver, ".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[4]/td[3]/a[1]");
		driver.findElement(saveButton).click();
		
	}
	
	public void verifyCustom(String text)
	{
		String customText= driver.findElement(customInfo).getText();
		System.out.println(customText);
		Assert.assertEquals(customText, text);
	}
	
	public void resetCustom()
	{
		ExplicitWait(driver, ".//*[@id='sidepage']/div/div[1]/div[1]/a[2]");;
		driver.findElement(resetButton).click();
	}
	
	public void uiCustomSO()
	{
		ExplicitWait(driver, ".//*[@id='salesorder-table-body']/tbody/tr[1]/td[1]/a");
		SoftAssert assertion= new SoftAssert();
		//driver.findElement(addButton).isDisplayed();
		assertion.assertTrue(driver.findElement(newestSO).isDisplayed(), "uiCustomSO Verified");		
		assertion.assertAll();
	}
	
	
}
