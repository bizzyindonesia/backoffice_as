package logistics.Pages;

import org.testng.Assert;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class BulkEditStatusPage 
{

	WebDriver driver;
	
	public BulkEditStatusPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
	By header= By.xpath(".//*[@id='content']/div/div[2]/div[1]/h2");
	By downloadButton= By.xpath(".//*[@id='bulk-update-list']/table/tbody/tr[5]/td[6]/a");
	By refreshButton= By.xpath(".//*[@id='content']/div/div[1]/ul/li/a");
	
	
	
	
	public void verifyBulkEditStatusPage()
	{
		String judul = driver.findElement(header).getText();
		Assert.assertEquals(judul, "Bulk Edit Status");
		Assert.assertEquals(driver.getTitle(), "Bizzy Back Office - Buyer");		
		
	}
	
	
}
