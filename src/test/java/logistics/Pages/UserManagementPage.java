package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class UserManagementPage 
{

	WebDriver driver;
	
	public UserManagementPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,100)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	public void ExplicitWaitText(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.linkText(text)));
	}
	
	By addUserButton= By.xpath(".//*[@id='users']/div[2]/div[1]/div/a");
	By editButton= By.linkText("Edit"); 
		By firstName= By.xpath(".//*[@id='sidepage']/div[1]/div[2]/div/form/div[1]/div[1]/div/input");
		By lastName= By.xpath(".//*[@id='sidepage']/div[1]/div[2]/div/form/div[1]/div[2]/div/input");
		By email= By.name("user-email");
		By role= By.xpath(".//*[@id='sidepage']/div[1]/div[2]/div/form/div[3]/div/select");
	
	By saveButton= By.xpath(".//*[@id='sidepage']/div[1]/div[1]/div[3]/input");		
	
	
	By rolesTab= By.linkText("ROLES");
		By checkRoleListSO= By.xpath(".//*[@id='list_so']");
		
	By permissionsTab= By.linkText("PERMISSIONS");	
		By headerPermission = By.xpath(".//*[@id='sidepage']/div[3]/div[1]/div[2]/h2");
		
	By popup= By.xpath("html/body/div[2]/span[3]");
	
	
	
	public void clickAddUser()
	{
		ExplicitWait(driver, ".//*[@id='users']/div[2]/div[1]/div/a");
		driver.findElement(addUserButton).click(); 
	}
	
	public void clickEditButton()
	{
		ExplicitWaitText(driver, "Edit");
		driver.findElement(editButton).click();
	}
	
	public void clickSaveButton()
	{
		driver.findElement(saveButton).click();
	}
	
	public void addUser(String first, String last, String mail, String roles)
	{		
		driver.findElement(firstName).sendKeys(first);
		driver.findElement(lastName).sendKeys(last);
		driver.findElement(email).sendKeys(mail);
		driver.findElement(role).sendKeys(roles);		
	}
	
	public void editUser(String first, String last,  String roles)
	{
		
		driver.findElement(firstName).sendKeys(first);
		driver.findElement(lastName).sendKeys(last);
		driver.findElement(role).sendKeys(roles);
	}
	
	public void clickRolesTab()
	{
		ExplicitWait(driver, ".//*[@id='users']/div[2]/div[1]/div/a");
		driver.findElement(rolesTab).click();
	}
	
	public void checkRole()
	{
		ExplicitWait(driver, ".//*[@id='list_so'");
		driver.findElement(checkRoleListSO).click();;
	}
	
	public void clickPermissionTab()
	{
		ExplicitWaitText(driver, "PERMISSIONS");
		driver.findElement(permissionsTab).click();
	}
	
	public void headerText(String headerT)
	{
		String permis= driver.findElement(headerPermission).getText();
		System.out.println(permis);
		Assert.assertEquals(permis, headerT);
	}
	
	public void verifyPopup(String popupShown)
	{
		ExplicitWait(driver, "html/body/div[2]/span[3]");
		String pop= driver.findElement(popup).getText();
		//System.out.println(pop);
		Assert.assertEquals(pop, popupShown);
	}
	
	
}
