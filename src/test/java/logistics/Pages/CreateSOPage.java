package logistics.Pages;

import org.openqa.selenium.By;
//import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.asserts.SoftAssert;

public class CreateSOPage 
{

	WebDriver driver;
	
	
	public CreateSOPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
		
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
		
	By customerSearch= By.xpath(".//*[@id='content']/div/div[2]/div/div[1]/div[1]/div/div[1]");
	By customerSeearch= By.className("selectize-input items not-full");
	By cust= By.id("cust");
	By PONumber= By.id("cart-po_number");
	By paymentTerms= By.id("cart-terms");
	By memo= By.id("cart-memo");
	By SKUSearch= By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/table/tbody/tr[2]/td[2]/div/div[1]");
	By SKU= By.xpath(".//section[@id='content']/div/div[3]/div/div/table/tbody/tr[2]/td[2]/div/div");
	By qty= By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/table/tbody/tr[2]/td[5]/input");
	By price= By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/table/tbody/tr[2]/td[6]/input");
	By tax= By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/table/tbody/tr[2]/td[7]/select");
	By addButton= By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/table/tbody/tr[2]/td[10]/a");
	By addButton1=	By.xpath(".//*[@id='content']/div/div[3]/div/div[1]/div/table/tbody/tr[2]/td[10]/button");
							  
	By createSOButton= By.xpath(".//*[@id='content']/div/div[3]/div/div[2]/div/a");
	
	
	public void addProduct()
	{
		
		ExplicitWait(driver, ".//*[@id='content']/div/div[2]/div/div[1]/div[1]/div/div[1]");
		//driver.findElement(SKU).click();
		//driver.findElement(customerSearch).sendKeys("PT. Bali Nirawan Resort");
		//driver.findElement(customerSearch).sendKeys(Keys.ENTER);
		
		driver.findElement(SKU).sendKeys("OAA000033 CRZ");
		/*
		driver.findElement(qty).sendKeys("2");
		driver.findElement(price).sendKeys("90000");
		driver.findElement(tax).sendKeys("10"); 
		driver.findElement(addButton).click();
		*/
	}
	
	public void uiCreateSO()
	{
		ExplicitWait(driver, ".//*[@id='content']/div/div[2]/div/div[1]/div[1]/div/div[1]");	
		SoftAssert assertion= new SoftAssert();
		//driver.findElement(addButton).isDisplayed();
		//assertion.assertTrue(driver.findElement(addButton1).isDisplayed());
                assertion.assertTrue(driver.findElement(addButton1).isDisplayed(), "Verified");
                
                assertion.assertAll();
	}
	
	
	
}
