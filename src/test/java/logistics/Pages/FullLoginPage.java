package logistics.Pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class FullLoginPage 
{

	WebDriver driver;
	
	By googleLogin= By.xpath(".//*[@id='l-google-login']/div/a/img");
	By email= By.id("Email");
	By nextButton= By.id("next");
	By staysign= By.id("PersistentCookie");
	By passwor= By.xpath(".//*[@id='Passwd']");
	By signInButton= By.id("signIn");
	By forgetPassword= By.id("link-forgot-passwd");
	
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.id(text)));
	}
	
	
	public FullLoginPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
	public void verifyLogin(String user, String passw)
	{
		driver.manage().timeouts().implicitlyWait(950, TimeUnit.SECONDS);
		driver.findElement(googleLogin).click();
		
		ExplicitWait(driver, "Email");
		driver.findElement(email).sendKeys(user);
		driver.findElement(nextButton).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//driver.findElement(staysign).click();
		//driver.findElement(forgetPassword).click();
		driver.findElement(passwor).sendKeys(passw);
		driver.findElement(signInButton).click();
		
	}
	
	public void validLoginAssert()
	{
		Assert.assertEquals(driver.getTitle(), "Bizzy Back Office");
	}
	
	public void invalidLoginAssert()
	{
		driver.findElement(googleLogin).isDisplayed();
		
		
	}
	
}
