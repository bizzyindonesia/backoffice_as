package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HomePage 
{

	WebDriver driver;
	
	By logoutButton= By.xpath(".//*[@id='header']/ul/li[3]/a");
	By menuTrigger= By.xpath(".//*[@id='menu-trigger']/div");
	
	public HomePage(WebDriver driver)
	{
		this.driver= driver;
	}
	
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	public void clickLogout()
	{
		driver.findElement(logoutButton).click();
	}
	
	public void clickMenuTriger()
	{
		ExplicitWait(driver, ".//*[@id='menu-trigger']/div");
		driver.findElement(menuTrigger).click();
	}
}
