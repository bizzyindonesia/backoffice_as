package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

public class BuyerPage 
{

	WebDriver driver;
	
	
	public BuyerPage(WebDriver driver)
	{
		this.driver= driver;
	}
	
		
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,50)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	By buyingListTab= By.linkText("BUYING LIST");
	By filterSO= By.xpath(".//*[@id='buying-list']/div[2]/div[1]/div[1]/input");
	By checkSKU = By.xpath(".//*[@id='buyinglist-table-body']/tbody/tr[1]/td[9]/input");
	By createPOButton= By.xpath(".//*[@id='createPO']/a");
	By linkSO= By.xpath(".//*[@id='table-bl-scroll']/table/tbody/tr[1]/td[5]/a");
	By memo= By.xpath(".//*[@id='txtMemo']");
	By saveButton= By.xpath(".//*[@id='sidepage']/div/div[1]/div[3]/button");
	By notif= By.xpath(".//*[@id='buyerApp']/div[2]/span[3]");
	By noTH= By.xpath(".//*[@id='buyinglist-table-header']/tbody/tr[1]/th[1]");
	
	
	public void clickBuyingList()
	{
		driver.findElement(buyingListTab).click();
	}
	
	public void chooseSO(String SONumber)
	{
		driver.findElement(filterSO).sendKeys(SONumber);
		driver.findElement(checkSKU).click();
	}
	
	public void clickCreatePO()
	{
		ExplicitWait(driver, ".//*[@id='createPO']/a");
		driver.findElement(createPOButton).click();
	}
	
	public void createNewPO()
	{
		
		driver.findElement(memo).sendKeys("test create po");
		driver.findElement(saveButton).click();
		
	}
	
	public void assertNotif(String text)
	{
		String popup= driver.findElement(notif).getText();
		Assert.assertEquals(popup, text);
	}
	
	public void uiBuyingList()
	{
		ExplicitWait(driver, ".//*[@id='buyinglist-table-header']/tbody/tr[1]/th[1]");
		SoftAssert assertion = new SoftAssert();
		assertion.assertTrue(driver.findElement(noTH).isDisplayed(),"uiBuyingList Verified");
		assertion.assertAll();
	}
	
	
	
}
