package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SideBar 
{
	WebDriver driver;
	
	public SideBar(WebDriver driver)
	{
		this.driver= driver;
	}
		
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	public void ExplicitWaitText(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.linkText(text)));
	}
	
	
	//By administrationMenu= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[1]/a");
		//By userManagement= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[1]/ul/li/a");
	
	By administrationMenu= By.linkText("Administration");
		By userManagement= By.linkText("User Management");
	
	By salesMenu= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[2]/a");
		By createSO= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[2]/ul/li[1]/a");
							   
		By customSO= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[2]/ul/li[2]/a");
							   
	
	By catalogMenu= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/a");
		By productManagement= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/a");
			By productList= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/ul/li[1]/a");
			By bulkUpload= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/ul/li[2]/a");
			By bulkEdit= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/ul/li[3]/a");
			By bulkEditStatus= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/ul/li[4]/a");
		By vendorManagement= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[2]/a");
	
	By orderMS= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/a");
		By buyer= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li/a");
		
	By logisticMenu= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/a");
		//By buyer= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[1]/a");
		By warehouse= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/a");
			By goodReceive= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[1]/a");
			By scanDocument= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[2]/a");
			By receiving= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[3]/a");
			By checking= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[4]/a");
			By putAway= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[5]/a");
			By picking= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[6]/a");
			By packing= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[7]/a");
			By shipping= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[2]/ul/li[8]/a");
		By inventory= By.xpath(".//*[@id='sidebar']/div[1]/ul/li[4]/ul/li[3]/a");
			
			
		
	public void clickBulkEditStatus()
	{
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[3]/a");
		driver.findElement(catalogMenu).click();
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/a");
		driver.findElement(productManagement).click();
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[3]/ul/li[1]/ul/li[4]/a");
		driver.findElement(bulkEditStatus).click();
	}
	
	
	public void clickCreateSO()
	{
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[2]/a");
		driver.findElement(salesMenu).click();
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[2]/ul/li[1]/a");
		driver.findElement(createSO).click();
	}
	
	public void clickCustomSO()
	{
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[2]/a");
		driver.findElement(salesMenu).click();
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[2]/ul/li[2]/a");
		driver.findElement(customSO).click();
	}
	
	public void clickUserManagement()
	{
		ExplicitWaitText(driver, "Administration");
		driver.findElement(administrationMenu).click();
		ExplicitWaitText(driver, "User Management");
		driver.findElement(userManagement).click();
	}
	
	public void clickLogistic()
	{
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[4]/a");
		driver.findElement(logisticMenu).click();
	}
	
	public void clickBuyer()
	{
		ExplicitWait(driver, ".//*[@id='sidebar']/div[1]/ul/li[4]/a");
		driver.findElement(orderMS).click();
		driver.findElement(buyer).click();
	}
	
}
