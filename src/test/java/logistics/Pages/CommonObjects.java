package logistics.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class CommonObjects 
{

	WebDriver driver;
	
	
	public CommonObjects(WebDriver driver)
	{
		this.driver= driver;
	}
	
		
	public void ExplicitWait(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.xpath(text)));
	}
	
	public void ExplicitWaitText(WebDriver driver, String text)
	{
		(new WebDriverWait(driver,10)).until(ExpectedConditions.elementToBeClickable(By.linkText(text)));
	}
		
	
	By saveButton= By.xpath(".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[4]/td[3]/a[1]");
	By resetButton= By.xpath(".//*[@id='sidepage']/div/div[1]/div[1]/a[2]");
	
	By customInfo= By.xpath(".//*[@id='table-so-scroll']/table/tbody/tr[1]/td[9]/span");
							 
	
	
	public void clickSaveButton()
	{	
		ExplicitWait(driver, ".//*[@id='sidepage']/div/div[3]/div[2]/table/tbody/tr[4]/td[3]/a[1]");
		driver.findElement(saveButton).click();
		
	}
	
	
}
